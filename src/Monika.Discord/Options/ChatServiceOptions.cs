namespace Monika.Options
{
    public class ChatServiceOptions
    {
        public string ApiKey { get; set; }
        public string ProjectId { get; set; }
    }
}