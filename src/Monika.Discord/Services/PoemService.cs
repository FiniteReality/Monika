using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Monika.Options;

namespace Monika.Services
{
    public partial class PoemService
    {
        private readonly string _programName;
        public PoemService(IOptions<PoemServiceOptions> options)
        {
            _programName = options.Value.Program;
        }

        public async Task<Stream> GenerateImageAsync(string contents,
            string font)
        {
            var proc = CreateStream(font);
            await proc.StandardInput.WriteAsync(contents)
                .ConfigureAwait(false);
            proc.StandardInput.Close();
            return new PoemStream(proc, proc.StandardOutput.BaseStream);
        }

        private Process CreateStream(string font)
        {
            return Process.Start(new ProcessStartInfo
            {
                FileName = _programName,
                Arguments = font,

                WorkingDirectory = Path.GetDirectoryName(
                    Path.GetFullPath(_programName)),

                UseShellExecute = false,

                RedirectStandardInput = true,
                RedirectStandardOutput = true
            });
        }
    }
}