#pragma warning disable IDISP007

using System;
using System.Diagnostics;
using System.IO;

namespace Monika.Services
{
    public partial class PoemService
    {
        private class PoemStream : Stream, IDisposable
        {
            private readonly Process _process;
            private readonly Stream _underlying;

            public PoemStream(Process process,
                Stream underlyingStream)
            {
                _process = process;
                _underlying = underlyingStream;
            }

            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                _process?.WaitForExit();
                _process?.Dispose();
                _underlying?.Dispose();
            }

            public override bool CanRead
                => _underlying.CanRead;

            public override bool CanSeek
                => _underlying.CanSeek;

            public override bool CanWrite
                => _underlying.CanWrite;

            public override long Length
                => _underlying.Length;

            public override long Position
            {
                get => _underlying.Position;
                set => _underlying.Position = value;
            }

            public override void Flush()
                => _underlying.Flush();

            public override int Read(byte[] buffer, int offset, int count)
                => _underlying.Read(buffer, offset, count);

            public override long Seek(long offset, SeekOrigin origin)
                => _underlying.Seek(offset, origin);

            public override void SetLength(long value)
                => _underlying.SetLength(value);

            public override void Write(byte[] buffer, int offset, int count)
                => _underlying.Write(buffer, offset, count);
        }
    }
}